﻿using System;
using System.Diagnostics;
using System.Runtime.Versioning;
using System.Threading.Tasks;

namespace ParallelPiCalculator
{
    class Program
    {
        private static readonly object lockObject = new object();

        static void Main(string[] args)
        {
            var iterations = 10000000;

            CalculatePi(iterations);

            for (var numberOfThreads = 1; numberOfThreads <= 4; numberOfThreads = 2*numberOfThreads)
            {
                CalculatePiParallel(iterations, numberOfThreads);
                CalculatePiParallelEfficient(iterations, numberOfThreads);
            }
            
            Console.Read();
        }


        static void CalculatePi(int iterations)
        {
            var sw = Stopwatch.StartNew();

            decimal sum = 0;
            for (var i = 0; i <= iterations; i++)
            {
                decimal upper = i % 2 == 0 ? 1 : -1;
                sum += upper / (2 * i + 1);
            }

            decimal pi = sum * 4;
            Console.WriteLine($"\n--- NORMAL ---");
            Console.WriteLine($"Time elapsed: {sw.Elapsed}");
//            Console.WriteLine($"Pi value: {pi}");
        }

        static void CalculatePiParallel(int iterations, int numberOfThreads)
        {
            var sw = Stopwatch.StartNew();

            decimal sum = 0;
            Parallel.For(0, iterations, new ParallelOptions {MaxDegreeOfParallelism = numberOfThreads}, (i, state) =>
            {
                decimal upper = i % 2 == 0 ? 1 : -1;
                lock (lockObject)
                {
                    sum += upper / (2 * i + 1);
                }
            });
            ;

            decimal pi = sum * 4;
            ;
            Console.WriteLine($"\n--- PARALLEL ---");
            Console.WriteLine($"Time elapsed: {sw.Elapsed}");
//            Console.WriteLine($"Pi value: {pi}");
            Console.WriteLine($"Number of threads: {numberOfThreads}");
        }

        static void CalculatePiParallelEfficient(int iterations, int numberOfThreads)
        {
            var sw = Stopwatch.StartNew();
            var tab = new decimal[iterations + 1];

            Parallel.For(0, iterations, new ParallelOptions {MaxDegreeOfParallelism = numberOfThreads}, (i, state) =>
            {
                decimal upper = i % 2 == 0 ? 1 : -1;
                tab[i] = upper / (2 * i + 1);
            });

            decimal sum = 0;
            for (var i = 0; i <= iterations; i++)
            {
                sum += tab[i];
            }

            decimal pi = sum * 4;
            Console.WriteLine($"\n--- PARALLEL EFFICIENT---");
            Console.WriteLine($"Time elapsed: {sw.Elapsed}");
//            Console.WriteLine($"Pi value: {pi}");
            Console.WriteLine($"Number of threads: {numberOfThreads}");

        }
    }
}